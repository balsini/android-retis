#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define MAXSIZE 32
#define MAXCPUS 128

#define SLEEPTIME_MS 500

void time_add_ms(struct timespec *t, int ms)
{
  t->tv_sec += ms/1000;
  t->tv_nsec += (ms%1000)*1000000;
  if (t->tv_nsec > 1000000000) {
    t->tv_nsec -= 1000000000;
    t->tv_sec += 1;
  }
}

struct cpu_stat {
    unsigned long long int user;
    unsigned long long int nice;
    unsigned long long int system;
    unsigned long long int idle;
    unsigned long long int iowait;
    unsigned long long int irq;
    unsigned long long int softirq;
    unsigned long long int steal;
    unsigned long long int guest;
    unsigned long long int guest_nice;

    struct cpu_stat *next;
};

unsigned long long int last_spent_time[MAXCPUS];

int main()
{
  char *line = NULL;
  FILE *fp;
  size_t len = 0;
  ssize_t read;
  char cpu[MAXSIZE];
  int count;
  struct cpu_stat current;
  struct cpu_stat *s;
  int cpus;
  int i;
  int first;
  struct cpu_stat *head;
  struct cpu_stat *tail;

  unsigned long long int total_time;
  unsigned long long int seconds;
  unsigned long long int cpu_usage;

  head = NULL;
  tail = NULL;

  for (;;) {
    struct timespec t;

    clock_gettime(CLOCK_MONOTONIC, &t);
    time_add_ms(&t, SLEEPTIME_MS);

    fp=fopen("/proc/stat", "r");
    if (fp == NULL)
      exit(EXIT_FAILURE);

    first = 1;
    cpus = 0;
    while ((read = getline(&line, &len, fp)) != -1) {
      if (first) {
        first = 0;
        continue;
      }

      //printf("Retrieved line of length %zu : \"%s\"", read, line);
      count = sscanf(line,
                     "%s "
                     "%d "
                     "%d "
                     "%d "
                     "%d "
                     "%d "
                     "%d "
                     "%d "
                     "%d "
                     "%d "
                     "%d ",
                     cpu,
                     &current.user, &current.nice, &current.system, &current.idle, &current.iowait,
                     &current.irq, &current.softirq, &current.steal, &current.guest, &current.guest_nice);

      if (strncmp(cpu, "cpu", 3))
        break;

      s = malloc(sizeof(struct cpu_stat));
      memcpy(s, &current, sizeof(current));
      s->next = NULL;
      if (tail == NULL) {
        tail = head = s;
      } else {
        tail->next = s;
        tail = s;
      }

      ++cpus;
    }

    /*
    for (s = head; s != NULL; s = s->next) {
      printf("%d %d %d %d %d %d %d %d %d %d\n",
             s->user, s->nice, s->system, s->idle, s->iowait,
             s->irq, s->softirq, s->steal, s->guest, s->guest_nice);
    }
    */

    /*
    printf("\n");
    printf("CPUs: %d\n", cpus);
*/
    for (i=0; i<cpus; ++i) {
      printf("CPU%d\t", i);
    }
    printf("\n");

    i=0;
    for (s = head; s != NULL; s = s->next) {
      unsigned long long int M = s->user + s->nice + s->system /*+ s->idle*/ + s->iowait +
                                 s->irq + s->softirq + s->steal + s->guest + s->guest_nice;
      unsigned long long int total_time = M * sysconf(_SC_CLK_TCK);
      unsigned long long int partial_time = total_time - last_spent_time[i];

      printf("%.2f\t", i + 1, 10.0 / SLEEPTIME_MS * partial_time);

      last_spent_time[i] = total_time;

      i++;
    }
    printf("\n");

    fflush(stdout);
    printf("\n\n\n");

    while (head != NULL) {
      tail = head->next;
      free(head);
      head = tail;
    }

    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL);
    time_add_ms(&t, SLEEPTIME_MS);
  }

  exit(EXIT_SUCCESS);
}
