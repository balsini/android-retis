#cp hikey-linaro/arch/arm64/boot/Image device/linaro/hikey-kernel

echo "hi6220-hikey.dtb  -->  ${HIKEY_KERNEL_DIR}/hi6220-hikey.dtb-4.9"
cp kernel-custom/default/arch/arm64/boot/dts/hisilicon/hi6220-hikey.dtb ${HIKEY_KERNEL_DIR}/hi6220-hikey.dtb-4.9

echo "Image-dtb  -->  ${HIKEY_KERNEL_DIR}/Image-dtb-4.9"
cp kernel-custom/default/arch/arm64/boot/Image-dtb ${HIKEY_KERNEL_DIR}/Image-dtb-4.9

. android-retis/script/init.sh

make bootimage -j12 TARGET_USERDATAIMAGE_4GB=true
