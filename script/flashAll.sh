INSTALLER_DIR=device/linaro/hikey/installer/hikey/
FIRMWARE_DIR=device/linaro/hikey/installer/hikey/
OUT_IMGDIR=out/target/product/hikey/

fastboot flash ptable "${INSTALLER_DIR}"/ptable-aosp-4g.img
fastboot flash fastboot "${FIRMWARE_DIR}"/fip.bin
fastboot flash nvme "${INSTALLER_DIR}"/nvme.img
fastboot flash boot "${OUT_IMGDIR}"/boot.img
fastboot flash system "${OUT_IMGDIR}"/system.img
fastboot flash cache "${OUT_IMGDIR}"/cache.img
fastboot flash userdata "${OUT_IMGDIR}"/userdata.img

#fastboot flash boot out/target/product/hikey/boot.img
#fastboot flash -w system out/target/product/hikey/system.img
#fastboot flash -w data out/target/product/hikey/userdata.img
fastboot reboot
